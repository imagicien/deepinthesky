public class Porte
{
  public color couleur = color(0, 0, 0);
  public float ouverture = 0;
  public float angle = 0;
  
  public Porte(color aCouleur, float aOuverture)
  {
    couleur = aCouleur;
    ouverture = aOuverture;
    angle = random(2*PI);
  }
}

public class Fruit
{
  public int type = 0;
  public float angle = 0;
  public float distance = 0;
  public float pivotX;
  public float pivotY;
  
  public Fruit(int aType, float aAngle)
  {
    type = aType;
    angle = aAngle;
    pivotX = fruitImages[type].width/2;
    pivotY = fruitImages[type].height/2;
  }
  
  float getX()
  {
    return distance * cos(angle);
  }
  
  float getY()
  {
    return distance * sin(angle);
  }
  
  void draw()
  {
    image(fruitImages[type], -pivotX, -pivotY);
  }
}

ArrayList<Porte> portes = new ArrayList<Porte>();
float ouvMax;
float demiOuvMax;
float hautPorte;
float yPorte;

float intervalle = 0.1;
float compteur = intervalle;
int lastMillis;

int nbCouleurs = 5;
int nbPalettes = 5;
color[][] couleurs = new color[][]{
  { #CE43A4, #FF0B4A, #A4D821, #C6F05E, #45C0BA }, // Electric Apple Tree
  { #E1FF00, #E60F5E, #E60F0F, #03E9C3, #FCFDF3 }, // Trippy1
  { #69D38D, #FF0000, #000000, #000000, #000000 }, // Scary1
  { #330521, #322A33, #4F3F40, #874F49, #FA1648 }, // scary & scarier
  { #630F2A, #3A0D1F, #1C1325, #023A47, #0F644D }  // TheDeepShadowFormula
};
int indiceCouleur = 0;
int indicePalette = 0;
Boolean random = false;

PImage[] fruitImages = new PImage[2];
ArrayList<Fruit> fruits = new ArrayList<Fruit>();
int downSampleFactor = 8;

float t = 0;

void setup()
{
  frameRate(60);
  size(800, 800, P2D);
  noStroke();
  colorMode(HSB, 1.0);
  
  ouvMax = sqrt( width*width + height*height );
  demiOuvMax = ouvMax/2;
  hautPorte = ouvMax * 2;
  yPorte = -hautPorte/2;
  
  fruitImages[0] = loadImage("framboise.png");
  fruitImages[1] = loadImage("banane.png");
  // Downsample
  for(PImage i : fruitImages)
  {
    i.resize(i.width/downSampleFactor, i.height/downSampleFactor);
  }
  
  //fruits.add(new Fruit(0, 0)); // Framboise
  //fruits.add(new Fruit(1, PI)); // Banane
  
  lastMillis = millis();
}

void draw()
{
  //background(0);
  
  float dt = (millis() - lastMillis) / 1000.0;
  lastMillis = millis();
  
  t += dt;
  
  compteur += dt;
  if(compteur >= intervalle)
  {
    portes.add(0, new Porte(getColor(), -1.1*intervalle));
    indiceCouleur = nextColorIndex();
    
    compteur = 0;
  }
  
  // 0. Init transfo
  translate(width/2, height/2); // origine au centre
  
  // 1. Traiter les portes
  for(Object o : portes) {
    Porte p = (Porte)o;
    
    // 1.1 Dessiner la porte
    draw_door(p); 
      
    // 1.2 Incrémenter l'ouverture de la porte
    p.ouverture += dt/4;
  }
  
  // 2. Traiter cas limite: porte complètement ouverte
  int last = portes.size()-1;
  if(last >= 0 && portes.get(last).ouverture > 1)
  {
    // 2.1 Enlever porte ouverte
    portes.remove(last);
  }
  
  // 3. Afficher les fruits
  for(Fruit f : fruits)
  {
    pushMatrix();
    translate(  f.distance * width*0.8 * cos(f.angle),
                f.distance * height*0.8 * sin(f.angle));
    scale(f.distance * 0.7 * downSampleFactor);
    rotate(t*PI*1.5 + f.angle);
    f.draw();
    popMatrix();
    f.distance += dt;
    f.angle += PI/2 * dt;
  }
  
  // 4. Annihilation Du Dernier Fruit
  last = fruits.size()-1;
  if(last >= 0 && fruits.get(last).distance > 1.5)
  {
    fruits.remove(last);
  }
}

void draw_door(Porte p)
{
  float ouv = max(p.ouverture, 0);
  float largPorte = (1-ouv) * demiOuvMax; // un côté seulement
  
  pushMatrix();
  rotate(p.angle);
    fill(p.couleur);
    rect(-demiOuvMax,       yPorte, largPorte, hautPorte);
    rect(ouv * demiOuvMax,  yPorte, largPorte, hautPorte);
  popMatrix();
}

color getColor()
{
  return couleurs[indicePalette][indiceCouleur];
}

color nextColor()
{
  return couleurs[indicePalette][nextColorIndex()];
}

int nextColorIndex()
{
  if(random)
    return (int)random(nbCouleurs);
  else 
    return (indiceCouleur < nbCouleurs-1) ? indiceCouleur + 1 : 0;
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == LEFT) {
      indicePalette = (indicePalette > 0) ? indicePalette - 1 : nbPalettes-1;
    } else if (keyCode == RIGHT) {
      indicePalette = (indicePalette < nbPalettes-1) ? indicePalette + 1 : 0;
    } else if (keyCode == UP) {
      random = true;
    } else if (keyCode == DOWN) {
      random = false;
    } 
  }
  else
  {
    float angleLancer = atan2(mouseY - height/2, mouseX - width/2);
    
    if (key == 'f')
    {
      fruits.add(new Fruit(0, angleLancer));
    }
    else if (key == 'g')
    {
      fruits.add(new Fruit(0, angleLancer));
      fruits.add(new Fruit(0, angleLancer + PI));
    }
    else if (key == 'b')
    {
      fruits.add(new Fruit(1, angleLancer));
    }
    else if (key == 'n')
    {
      fruits.add(new Fruit(1, angleLancer + PI/2));
      fruits.add(new Fruit(1, angleLancer + 3*PI/2));
    }
  }
}
